import os
import time
import sys
import logging
import telegram
from urllib.request import urlopen
from bs4 import BeautifulSoup
from datetime import datetime
from telegram.ext import Updater
from telegram.ext import CommandHandler
from telegram.ext import MessageHandler, Filters


logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',level=logging.INFO)
logger = logging.getLogger(__name__)

chave = "Inserir a chave aqui"
egydiobot = telegram.Bot(token = chave)
atualizador = Updater(token = chave)
carteiro = atualizador.dispatcher



def getTime():
    tempo = str(datetime.utcnow())
    tempo = tempo.replace("-", " ")
    tempo = tempo.replace(":", " ")
    tempo = tempo.replace(".", " ")
    tempo = tempo.split(" ")
    tempo = "".join(tempo[:4])
    return tempo



def start(bot, atualizacao):

	atualizacao.message.reply_text("Olá! Eu sou o Santos Dubot e vou te passar informações de METAR,TAF e AVISOS DE AERÓDROMO. #vamosvoar")

def conversa(bot, atualizacao):
    
    tafEnviar = ""
    metarEnviar = ""
    woEnviar = ""    
    
    mensagem = atualizacao.message.text.strip()

    print(mensagem)
                  
    metar_url = "http://www.redemet.aer.mil.br/api/consulta_automatica/index.php?local="+ mensagem +"&msg=metar&data_ini=" + getTime() 
    taf_url ="http://www.redemet.aer.mil.br/api/consulta_automatica/index.php?local="+ mensagem +"&msg=taf&data_ini=" + getTime() 
    aviso_url = "http://www.redemet.aer.mil.br/api/consulta_automatica/index.php?local="+ mensagem +"&msg=aviso_aerodromo&data_ini=" + getTime()
   
    
    url_corrente = urlopen(metar_url)
    sopa_metar = BeautifulSoup(url_corrente.read(), "html.parser")
    
    url_corrente = urlopen(taf_url)
    sopa_taf = BeautifulSoup(url_corrente.read(), "html.parser")
    
    url_corrente = urlopen(aviso_url)
    sopa_aviso  = BeautifulSoup(url_corrente.read(), "html.parser")
    
    
    metarEnviar = "METAR: "+ sopa_metar.getText()
    tafEnviar = "TAF: " + sopa_taf.getText()
    woEnviar = "Aviso de aeródromo: " + sopa_aviso.getText()
    
    
    
    
    atualizacao.message.reply_text("Você vai receber informações do TAF;METAR e WO para o "+ mensagem.upper())
    atualizacao.message.reply_text(metarEnviar)
    atualizacao.message.reply_text(tafEnviar)
    atualizacao.message.reply_text(woEnviar)
    
    #print(metarEnviar+"\n"+tafEnviar+"\n"+woEnviar)
    #
    #atualizacao.message.reply_text("Aviso de aeródromo(SBGL): " + sopa_aviso2.getText())

        
    
def restart(bot, update):

    egydiobot.send_message(update.message.chat_id, "Falou!")
    time.sleep(0.2)
    os.execl(sys.executable, sys.executable, *sys.argv)
    
    


carteiro.add_handler(CommandHandler('r', restart))

evento_start = CommandHandler("start", start)
carteiro.add_handler(evento_start)

recebedor = MessageHandler(Filters.text | Filters.photo, conversa)

carteiro.add_handler(recebedor)

if atualizador.start_polling():

	print("Bot atualizado com sucesso!")

else:

	print("Falha na atualização do bot!")
 
atualizador.idle()
